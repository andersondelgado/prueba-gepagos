<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuariosPagos extends Model
{
    //
    protected $table="usuariospagos";

    public $timestamps = false;

    protected $primaryKey = 'codigousuario';

    protected $fillable = ['codigousuario','codigopago'];

    public function users(){
        return $this->belongsTo('App\Usuarios','codigousuario');
    }

    public function pay(){
        return $this->belongsTo('App\Pagos','codigopago');
    }
}
