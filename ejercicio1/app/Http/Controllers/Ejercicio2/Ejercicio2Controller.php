<?php

namespace app\Http\Controllers\Ejercicio2;

use App\Usuarios;
use App\UsuariosPagos;
use App\Pagos;
use App\Favoritos;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class Ejercicio2Controller extends Controller
{
    private $crud = ["read", "create", "edit", "update", "delete"];

    public function login(Request $request)
    {
        $usuario = $request->input('usuario');
        $clave = $request->input('clave');

        $usr = Usuarios::where('usuario', $usuario)->where('clave', $clave)->first();

        if (count($usr) > 0) {
            \Session::put('id', $usr->codigousuario);
            \Session::put('usuario', $usr->usuario);

            $id = \Session::get('id');
            $usuarioname = \Session::get('usuario');


            $array = array(
                'id' => $id,
                'usuario' => $usuarioname
            );
            return \Response::json($array);

        } else {
            $array = array(
                'error' => 'error',
                'result' => 'No coinciden las credenciales'
            );
            return \Response::json($array);
        }
    }

    public function usuariosCRUD($parameter, $id, Request $request)
    {
        $model = new Usuarios();
        if ($parameter == $this->crud[0]) {
            return $this->crudModel($parameter, $model, null, null, null, null);
        } elseif ($parameter == $this->crud[1]) {
            $age = $request->input('edad');
            $validator = \Validator::make($request->all(), [
                'usuario' => 'required|unique:usuarios',
            ]);

            if ($validator->fails()) {
                $array = array(
                    'error' => 'error',
                    'result' => 'Debe ser unico el usuario'
                );
                return \Response::json($array);
            } else if ($age < 18) {
                $array = array(
                    'error' => 'error',
                    'result' => 'No puede ser menor de edad'
                );
                return \Response::json($array);
            } else {
                $payload = $request->all();
                return $this->crudModel($parameter, $model, null, $payload, null, null);
            }

        } elseif ($parameter == $this->crud[2]) {
            $compare = "codigousuario";
            return $this->crudModel($parameter, $model, null, null, $compare, $id);

        } elseif ($parameter == $this->crud[3]) {
            $compare = 'codigousuario';
            $payload = $request->all();
            return $this->crudModel($parameter, $model, null, $payload, $compare, $id);

        } elseif ($parameter == $this->crud[4]) {
            $compare = 'codigousuario';
            return $this->crudModel($parameter, $model, null, null, $compare, $id);

        } else {

        }
    }

    public function favoritosCRUD($parameter, $id, Request $request)
    {
        $model = new Favoritos();
        if ($parameter == $this->crud[0]) {
            return $this->crudModel($parameter, $model, 'user', null, null, null);
        } elseif ($parameter == $this->crud[1]) {
            $validator = \Validator::make($request->all(), [
                'codigousuariofavorito' => 'required|unique:favoritos',
            ]);

            if ($validator->fails()) {
                $array = array(
                    'error' => 'error',
                    'result' => 'Debe ser unico el usuario'
                );
                return \Response::json($array);

            } else {
                $payload = $request->all();
                return $this->crudModel($parameter, $model, null, $payload, null, null);
            }

        } elseif ($parameter == $this->crud[2]) {
            $compare = "codigousuariofavorito";
            return $this->crudModel($parameter, $model, 'user', null, $compare, $id);

        } elseif ($parameter == $this->crud[3]) {
            $compare = 'codigousuariofavorito';
            $payload = $request->all();
            return $this->crudModel($parameter, $model, null, $payload, $compare, $id);

        } elseif ($parameter == $this->crud[4]) {
            $compare = 'codigousuariofavorito';
            return $this->crudModel($parameter, $model, null, null, $compare, $id);

        } else {

        }
    }

    public function insertUsuariosPagos(Request $request)
    {
        $codigousuario = \Session::get('id');
        $importe = $request->input('importe');
        $fecha = $request->input('fecha');
        $fecha=date_create($fecha);
        $fecha=date_format($fecha,'Y-m-d');
        $fechafin = date("Y-m-d");

        if ($fecha < $fechafin) {
            $array = array(
                'error' => 'error',
                'result' => 'Debe cumplir la fecha de pago, y hoy es: ' . $fechafin
            );
            return \Response::json($array);
        } else
            if ($importe <= 0) {
                $array = array(
                    'error' => 'error',
                    'result' => 'No puede ser menor de cero el importe'
                );
                return \Response::json($array);
            } else {
                $payload = array(
                    'importe'=>$importe,
                    'fecha'=>$fecha
                );
                $idpagos = Pagos::create($payload)->codigopago;
                $usrpago=UsuariosPagos::create(array(
                    'codigopago'=>$idpagos,
                    'codigousuario'=>$codigousuario
                ));

                if($usrpago){
                    $array = array(
                        'success' => '200',
                        'result' => 'insert exitoso..... '
                    );
                    return \Response::json($array);
                }else{
                    $array = array(
                        'error' => 'error',
                        'result' => 'fallo el insert'
                    );
                    return \Response::json($array);
                }

            }
    }

    public function pagosCRUD($parameter, $id, Request $request)
    {
        $model = new Pagos();
        if ($parameter == $this->crud[0]) {
            return $this->crudModel($parameter, $model, 'userpay', null, null, null);
        } elseif ($parameter == $this->crud[1]) {

            $importe = $request->input('importe');
            $fecha = $request->input('fecha');
            $fechafin = date("Y-m-d");

            if ($fecha < $fechafin) {
                $array = array(
                    'error' => 'error',
                    'result' => 'Debe cumplir la fecha de pago, y hoy es: ' . $fechafin
                );
                return \Response::json($array);
            } else
                if ($importe <= 0) {
                    $array = array(
                        'error' => 'error',
                        'result' => 'No puede ser menor de cero el importe'
                    );
                    return \Response::json($array);
                } else {
                    $payload = $request->all();
                    return $this->crudModel($parameter, $model, null, $payload, null, null);
                }

        } elseif ($parameter == $this->crud[2]) {
            $compare = "codigopago";
            return $this->crudModel($parameter, $model, null, null, $compare, $id);

        } elseif ($parameter == $this->crud[3]) {
            $compare = 'codigopago';
            $payload = $request->all();
            return $this->crudModel($parameter, $model, null, $payload, $compare, $id);

        } elseif ($parameter == $this->crud[4]) {
            $compare = 'codigopago';
            return $this->crudModel($parameter, $model, null, null, $compare, $id);

        } else {

        }
    }

    public function usuariospagosCRUD($parameter, $id, Request $request)
    {
        $model = new UsuariosPagos();
        if ($parameter == $this->crud[0]) {
           // return $this->crudModel($parameter, $model, null, null, null, null);
            return $model->with('users')->with('pay')->get();
        } elseif ($parameter == $this->crud[1]) {
            $payload = $request->all();
            return $this->crudModel($parameter, $model, null, $payload, null, null);

        } elseif ($parameter == $this->crud[2]) {
            $compare = "codigopago";
            return $this->crudModel($parameter, $model, null, null, $compare, $id);

        } elseif ($parameter == $this->crud[3]) {
            $compare = 'codigopago';
            $payload = $request->all();
            return $this->crudModel($parameter, $model, null, $payload, $compare, $id);

        } elseif ($parameter == $this->crud[4]) {
            $compare = 'codigopago';
            return $this->crudModel($parameter, $model, null, null, $compare, $id);

        } else {

        }
    }

    private function crudModel($parameter, $model, $with, $payload, $compare, $id)
    {

        if ($parameter == $this->crud[0]) {
            //with('state')
            if ($with != null) {
                $m = $model->with($with)->get();
            } else {
                $m = $model->get();
            }

            $model = $m;
        } elseif ($parameter == $this->crud[1]) {
            $model::create($payload);
        } elseif ($parameter == $this->crud[2]) {

            if ($with != null) {
                $m = $model->with($with)->where($compare, $id)->first();
            } else {
                $m = $model->where($compare, $id)->first();
            }
            $model = $m;

        } elseif ($parameter == $this->crud[3]) {
            $model->where($compare, $id)->update($payload);
        } elseif ($parameter == $this->crud[4]) {
            $model->where($compare, $id)->delete();
        }

        return $model;

    }


}