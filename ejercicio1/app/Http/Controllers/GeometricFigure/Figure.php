<?php
namespace app\Http\Controllers\GeometricFigure;

interface Figure{
    public function triangle($base,$height);
    public function square($a);
    public function circle($d);
}