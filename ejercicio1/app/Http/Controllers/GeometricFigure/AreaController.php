<?php

namespace app\Http\Controllers\GeometricFigure;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeometricFigure\Figure;
use Illuminate\Http\Request;


class AreaController extends Controller implements Figure
{

    public function typeFigure($type,Request $request){
        try{
            $result=null;
            switch ($type){
                case "triangle":
                    $base=$request->input('base');
                    $height=$request->input('height');
                    $result=$this->triangle($base,$height);
                    break;
                case "square":
                    $a=$request->input('side');
                    $result=$this->square($a);
                    break;
                case "circle":
                    $diameter=$request->input('diameter');
                    $result=$this->circle($diameter);
                    break;
                default:
                    $result= "Figure no found";
                    break;
            }

            $array=array(
                'area'=>$type,
                'result'=>$result
            );

            return \Response::json($array);
        }catch (\Exception $e){
            $array=array(
                'error'=>'error',
                'result'=>'Figure error...: '.$e
            );
            return \Response::json($array);
        }
    }


    public function triangle($base, $height)
    {
        // TODO: Implement triangle() method.
        $triangle=($base*$height)/2;
        return $triangle;
    }

    public function square($a)
    {
        // TODO: Implement square() method.
        $square=pow($a,2);
        return $square;
    }

    public function circle($d)
    {
        // TODO: Implement circle() method.
        $rr = pow($d,2);
        $rr=$rr/4;
        $circle = M_PI * $rr;
        return $circle;
    }


}