<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favoritos extends Model
{
    //
    protected $table="favoritos";

    protected $primaryKey = 'codigousuariofavorito';

    public $timestamps = false;

    protected $fillable = ['codigousuario','codigousuariofavorito'];

    public function user(){
        return $this->belongsTo('App\Usuarios','codigousuariofavorito');
    }
}
