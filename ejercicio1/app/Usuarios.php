<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    //
    protected $table="usuarios";

    protected $primaryKey = 'codigousuario';

    public $timestamps = false;

    protected $fillable = ['codigousuario','usuario','clave','edad'];


}
