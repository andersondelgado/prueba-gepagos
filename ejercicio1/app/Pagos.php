<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagos extends Model
{
    //
    protected $table="pagos";

    protected $primaryKey = 'codigopago';

    public $timestamps = false;

    protected $fillable = ['codigopago','importe','fecha'];

    public function userpay(){
        return $this->belongsTo('App\UsuariosPagos','codigopago')
            //->using('App\Usuarios')
        ;
    }
}
