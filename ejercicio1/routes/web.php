<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//-------inicio::--------ejercicio 1-------------------------------------------------------//
Route::get('/ejercicio1', function () {
    return view('area');
});

Route::get('/ejercicio2', function () {
    return view('ejercicio2');
});

Route::get('/authenticated', function () {
    return view('auth');
});

Route::post('/area/{type}/','GeometricFigure\AreaController@typeFigure');

Route::get('/area/circle/{r}','GeometricFigure\AreaController@circle');

Route::get('/area/square/{s}','GeometricFigure\AreaController@square');

Route::get('/area/triangle/{b}/{h}','GeometricFigure\AreaController@triangle');

//------fin::----------ejercicio1-----------------------------------------------------------//

//------inicio::-------ejercicio2----------------------------------------------------------//
Route::post('/login','Ejercicio2\Ejercicio2Controller@login');

Route::get('/logout',function (){
    \Session()->forget('id');
    \Session()->forget('usuario');
    \Session()->flush();
});

Route::get('/usuario/{parameter}/{id}','Ejercicio2\Ejercicio2Controller@usuariosCRUD');
Route::post('/usuario/{parameter}/{id}','Ejercicio2\Ejercicio2Controller@usuariosCRUD');

Route::post('/insertUsuariosPagos/','Ejercicio2\Ejercicio2Controller@insertUsuariosPagos');

Route::get('/pagos/{parameter}/{id}','Ejercicio2\Ejercicio2Controller@pagosCRUD');
Route::post('/pagos/{parameter}/{id}','Ejercicio2\Ejercicio2Controller@pagosCRUD');

Route::get('/usuariospagos/{parameter}/{id}','Ejercicio2\Ejercicio2Controller@usuariospagosCRUD');
Route::post('/usuariospagos/{parameter}/{id}','Ejercicio2\Ejercicio2Controller@usuariospagosCRUD');

Route::get('/favoritos/{parameter}/{id}','Ejercicio2\Ejercicio2Controller@favoritosCRUD');
Route::post('/favoritos/{parameter}/{id}','Ejercicio2\Ejercicio2Controller@favoritosCRUD');
//------fin::---------ejercicio2----------------------------------------------------------//