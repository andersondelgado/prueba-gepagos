<!DOCTYPE html>
<html ng-app="app">
<head>
    <title>ejercicio2</title>
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap-theme.css') }}">
</head>
<body ng-controller="homeCrt">

<div class="container">
    <div class="container-fluid">

    </div>
    <br>
    <div class="row">
        <div class="panel panel-default">
            home
            <br>
            <% usuario %>
            <button class="btn btn-default" ng-click="close()">Salir</button>
            <div class="text-info text-left">
                <ul ng-repeat="i in favo">
                    <li>Favoritos: <% i.user.usuario %>
                        <button class="btn btn-primary btn-group-sm" ng-click="removeFavorito(i.codigousuariofavorito)">
                            Quitar de favoritos
                        </button>
                    </li>
                </ul>
            </div>
            <br>
            <button class="btn btn-default btn-group-sm" ng-click="showFavo()">favoritos</button>
            <button class="btn btn-default btn-group-sm" ng-click="showPago()">Pagos</button>
            <div ng-show="showfavobool">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Usuarios</th>
                        <th>Accion</th>
                    </tr>
                    </thead>
                    <tbody ng-repeat="i in listUsr">
                    <tr ng-if="i.usuario!=usuario">
                        <td><% i.usuario %></td>
                        <td>
                            <button class="btn btn-primary btn-group-sm" ng-click="addFavorito(i.codigousuario)">Agregar
                                a
                                favorito
                            </button>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div ng-show="showpagobool">
                <br>
                 <div class="input-group">
                     <input type="text" class="form-control" placeholder="Importe" ng-model="model.importe">
                 </div>
                <div class="input-group">
                    <input type="date" class="form-control" placeholder="fecha" ng-model="model.fecha">
                </div>
                <button class="btn btn-primary btn-group-sm" ng-click="payment()">Pagar</button>
                <div class="text-danger" ng-if="message!=undefined">
                    <%message%>
                </div>
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Usuarios</th>
                        <th>Importe</th>
                        <th>Fecha</th>
                    </tr>
                    </thead>
                    <tbody ng-repeat="i in pay">
                    <tr ng-if="i.users.usuario==usuario">
                        <td><% i.users.usuario %></td>
                        <td><%i.pay.importe %></td>
                        <td><% i.pay.fecha | date:"dd/MM/yyyy 'at' h:mm:ss a" %></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
<script src="{{ asset('bower_components/angular-route/angular-route.min.js') }}"></script>
<script src="{{ asset('bower_components/ngstorage/ngStorage.min.js') }}"></script>
<script>
    var app = angular.module("app", ['ngStorage'], function ($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    app.controller("homeCrt", function ($scope, $http, $localStorage) {

        if ($localStorage.id == undefined || $localStorage.id == null) {
            window.location = "/ejercicio2";
        }


        $scope.usuario = $localStorage.usuario;
        var urluser = "<?php echo URL::to('usuario'); ?>";

        $scope.showfavobool = false;
        $scope.showpagobool = false;
        $scope.showFavo = function () {
            $scope.showfavobool = true;
            $scope.showpagobool = false;
        }

        $scope.showPago = function () {
            $scope.showfavobool = false;
            $scope.showpagobool = true;
        }

        $scope.listUser = function () {
            urluser += "/read/0/"
            $http.get(urluser).then(function (xhr) {
                $scope.listUsr = xhr.data;
            })
        }

        $scope.addFavorito = function (i) {
            var urlfavo = "<?php echo URL::to('favoritos'); ?>";
            urlfavo = urlfavo + "/create/0/";
            var send = {'codigousuario': $localStorage.id, 'codigousuariofavorito': i};
            $http.post(urlfavo, send).then(function () {
                location.reload();
            })
        }

        $scope.listFavo = function () {
            var urlfavo = "<?php echo URL::to('favoritos'); ?>";
            urlfavo += "/read/0/";
            $http.get(urlfavo).then(function (xhr) {
                $scope.favo = xhr.data;
            })
        }

        $scope.listPay = function () {
            var urlpago = "<?php echo URL::to('usuariospagos'); ?>";
            urlpago += "/read/0/";
            $http.get(urlpago).then(function (xhr) {
                $scope.pay = xhr.data;
            })
        }

        $scope.removeFavorito = function (i) {
            var urlfavo = "<?php echo URL::to('favoritos'); ?>";
            var urlfavos = urlfavo + "/delete/" + i + "/";
            $http.get(urlfavos).then(function () {
                location.reload();
            })
        }

        $scope.payment=function () {
            var urlpayment="<?php echo URL::to('insertUsuariosPagos')?>";
            $http.post(urlpayment,$scope.model).then(function (xhr) {
               // location.reload();
                if(xhr.data.error){
                    $scope.message=xhr.data.result;
                }else{
                    location.reload();
                }
            },function (error) {

            })
        }

        $scope.close=function () {
            $http.get('logout').then(function () {
                $localStorage.id=undefined;
                $localStorage.usuario=undefined;
                window.location="/ejercicio2";
            })
        }

        $scope.listPay();
        $scope.listUser();
        $scope.listFavo();


    });
</script>
</body>
</html>