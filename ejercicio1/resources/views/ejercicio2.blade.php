<!DOCTYPE html>
<html ng-app="app">
<head>
    <title>ejercicio2</title>
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap-theme.css') }}">
</head>
<body ng-controller="loginCrt">


<div class="container">

    <div class="row">
        <br>
        <div class="col-lg-10">
            <h3><a href="/">Volver al Inicio</a></h3>

            <button class="btn btn-default" ng-click="activeLogin()">Login</button>
            |
            <button class="btn btn-default" ng-click="activeReg()">Registrar</button>
        </div>
        <div ng-show="log" class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <form class="form-signin">
                <h2 class="form-signin-heading">Login</h2>
                <label for="inputEmail" class="sr-only">Usuario</label>
                <input type="text" id="inputEmail" ng-model="model.usuario" class="form-control"
                       placeholder="Email address" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" ng-model="model.clave" class="form-control"
                       placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" ng-click="login()" type="submit">Sign in</button>
            </form>
        </div>
        <br>
        <div ng-show="reg" class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <form class="form-signin">
                <h2 class="form-signin-heading">Registrar</h2>
                <label for="inputEmail" class="sr-only">Usuario</label>
                <input type="text" id="inputEmail" ng-model="models.usuario" class="form-control"
                       placeholder="Usuario" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" ng-model="models.clave" class="form-control"
                       placeholder="Password" required>
                <label for="inputEmail" class="sr-only">Edad</label>
                <input type="text" id="inputEmail" ng-model="models.edad" class="form-control" placeholder="Edad"
                       required autofocus>
                <button class="btn btn-lg btn-primary btn-block" ng-click="registrar()" type="submit">registrar</button>
                <br>
                <div class="text-danger" ng-if="message!=undefined">
                    <%message%>
                </div>
            </form>
        </div>
    </div>

</div>


<script src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
<script src="{{ asset('bower_components/angular-route/angular-route.min.js') }}"></script>
<script src="{{ asset('bower_components/ngstorage/ngStorage.min.js') }}"></script>
<script>
    var app = angular.module("app", ['ngStorage'], function ($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    app.controller("loginCrt", function ($scope, $http, $localStorage) {

        if ($localStorage.id != undefined || $localStorage.id != null) {
            window.location = "/authenticated";
        }

        var url = "<?php echo URL::to('login'); ?>";
        $scope.log = false;
        $scope.reg = false;
        $scope.activeLogin = function () {
            $scope.log = true;
            $scope.reg = false;
        }

        $scope.activeReg = function () {
            $scope.log = false;
            $scope.reg = true;
        }
        
        $scope.registrar=function(){
            var urlreg="<?php echo URL::to('usuario') ?>";
            urlreg+="/create/0/"
            $http.post(urlreg,$scope.models).then(function (xhr) {

                if(xhr.data.error){
                    $scope.message=xhr.data.result;
                }else {
                    location.reload();
                }
            })
        }
        
        $scope.login = function () {
            $http.post(url, $scope.model).then(function (xhr) {
                //console.log("xhr::: "+JSON.stringify(xhr.data));
                $localStorage.id = xhr.data.id;
                $localStorage.usuario = xhr.data.usuario;
                window.location = "/authenticated";
            }, function (err) {

            });
        }
    });
</script>
</body>
</html>