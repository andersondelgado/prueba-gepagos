<!DOCTYPE html>
<html ng-app="app">
<head>
    <title>ejercicio 1</title>
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap-theme.css') }}">
</head>
<body>
<div class="container">
    <div class="row">
        <div ng-controller="areaCtrl" class="boxed boxed--border">
            <h2>Figuras geometricas</h2>
            <h3><a href="/">Volver al Inicio</a></h3>
            <br>
            <div class="col-sm-4">
                <input type="radio" name="area" ng-click="circle()">Circulo
            </div>
            <div class="col-sm-4">
                <input type="radio" name="area" ng-click="square()">Cuadrado
            </div>
            <div class="col-sm-4">
                <input type="radio" name="area" ng-click="triangle()">Triangulo
            </div>
            <div class="modal-dialog" style="margin-left: 34%; margin-top: 12%;">
                <div ng-show="cr">

                    <div class="input-group input-group-sm">

                            <div class="col-sm-12">
                                <input type="text" class="form-control"  title="Introduzca valor" ng-model="model.diameter"
                                       placeholder="Valor del diametro">
                                <br><br>
                                <button class="btn btn-primary" ng-click="postArea('circle')">enviar</button>
                                <div><% area %> <% result %></div>
                            </div>


                    </div>
                </div>
                <div ng-show="sq">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control"  title="Introduzca valor" ng-model="model.side"
                               placeholder="Valor de la superficie">
                        <br><br>
                        <button class="btn btn-primary" ng-click="postArea('square')">enviar</button>
                        <div><% area %> <% result %></div>
                    </div>
                </div>
                <div ng-show="tr">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" title="Introduzca valor" ng-model="model.base"
                               placeholder="Valor de la base">
                        <br><br>
                        <input type="text" class="form-control"  title="Introduzca valor" ng-model="model.height"
                               placeholder="Valor de la altura">
                        <br><br>
                        <button class="btn btn-primary" ng-click="postArea('triangle')">enviar</button>
                        <div><% area %> <% result %></div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<script src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
<script>
    var app = angular.module("app", [], function ($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
    app.controller("areaCtrl", function ($scope, $http) {
        $scope.tr = false;
        $scope.sq = false;
        $scope.cr = false;
        $scope.area = null;
        $scope.result = null;
        $scope.triangle = function () {
            $scope.tr = true;
            $scope.sq = false;
            $scope.cr = false;

            $scope.area = null;
            $scope.result = null;
        }

        $scope.circle = function () {
            $scope.cr = true;
            $scope.tr = false;
            $scope.sq = false;

            $scope.area = null;
            $scope.result = null;
        }

        $scope.square = function () {
            $scope.sq = true;
            $scope.tr = false;
            $scope.cr = false;

            $scope.area = null;
            $scope.result = null;
        }

        $scope.postArea = function (type) {
            var url = "<?php echo URL::to('area'); ?>";
            var uri = url + "/" + type + "/";
            $http.post(uri, $scope.model).then(function (success) {
                $scope.area = success.data.area;
                $scope.result = success.data.result;
            }, function (error) {
                console.log("err");
                //$scope.area;
            });
        }

    });
</script>
</body>
</html>