-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-10-2017 a las 02:28:44
-- Versión del servidor: 5.7.19-log
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ejercicio2db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoritos`
--

CREATE TABLE `favoritos` (
  `codigousuariofavorito` int(11) DEFAULT NULL,
  `codigousuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `favoritos`
--

INSERT INTO `favoritos` (`codigousuariofavorito`, `codigousuario`) VALUES
(6, 5),
(4, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `codigopago` int(11) NOT NULL,
  `importe` double(8,2) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`codigopago`, `importe`, `fecha`) VALUES
(5, 34.00, '2017-10-03 00:00:00'),
(6, 90.00, '2017-10-03 00:00:00'),
(7, 34.00, '2017-10-04 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `codigousuario` int(11) NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clave` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edad` smallint(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`codigousuario`, `usuario`, `clave`, `edad`) VALUES
(4, 'anders', '1234567', 20),
(5, 'ander', '123456', 30),
(6, 'andersd', '123456', 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariospagos`
--

CREATE TABLE `usuariospagos` (
  `codigopago` int(11) DEFAULT NULL,
  `codigousuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuariospagos`
--

INSERT INTO `usuariospagos` (`codigopago`, `codigousuario`) VALUES
(5, 5),
(6, 5),
(7, 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD KEY `fk_favoritos_1_idx` (`codigousuariofavorito`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`codigopago`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`codigousuario`);

--
-- Indices de la tabla `usuariospagos`
--
ALTER TABLE `usuariospagos`
  ADD KEY `fk_usuariospagos_1_idx` (`codigousuario`),
  ADD KEY `fk_usuariospagos_2_idx` (`codigopago`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `codigopago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `codigousuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD CONSTRAINT `fk_favoritos_1` FOREIGN KEY (`codigousuariofavorito`) REFERENCES `usuarios` (`codigousuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuariospagos`
--
ALTER TABLE `usuariospagos`
  ADD CONSTRAINT `fk_usuariospagos_1` FOREIGN KEY (`codigousuario`) REFERENCES `usuarios` (`codigousuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuariospagos_2` FOREIGN KEY (`codigopago`) REFERENCES `pagos` (`codigopago`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
