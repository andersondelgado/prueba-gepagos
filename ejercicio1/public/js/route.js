app.config(function ($routeProvider) {

    $routeProvider       
        .when('/', {
            templateUrl: 'view/home.html',
            controller: 'homeCrt'
        })
        .when('/logout', {
            templateUrl: 'view/exit.html',
            controller: 'logoutCrt'
        })
        .otherwise({
            redirectTo: '/'
        });
});
