Prueba Geopagos


**Pre-requisitos:**
> Instalar xampp(Windows, Linux, MacOS)
    
> Instalar composer (Windows, Linux, MacOS)

**Ejecucion del programa:**
> Restaurar base de datos que esta en el directorio /ejercicio1/script-sql/ejercicio2db.sql/ejercicio2db.sql

> Copiar archivo .env.example a .env
ejecutar comando     **php artisan key:generate**

> Confuguar nombre de la base de datos de la siguiente forma en el archivo .env
> 
**> DB_CONNECTION=mysql

> DB_HOST=localhost

> DB_PORT=3306

> DB_DATABASE=ejercicio2db

> DB_USERNAME=root

> DB_PASSWORD=******
> 
> Ejecutar comando para levantar servidor de prueba: **php artisan serve** y por defecto levantara el **http://locahost:8000/**

> Hacer click a los enlaces que aparecen en la imagen a continuacion
 ![prueba-geopago.png](https://bytebucket.org/andersondelgado/prueba-gepagos/raw/0c1194e0eef91e914f236da0f82a3a7c0b179198/prueba-geopago.png?token=7fb183d36cea095932950e53205452d5f3da5fa2 "")
